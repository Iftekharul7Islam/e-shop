<?php
session_start();

include_once("../src/db.php");

$product_id = $_GET['product_id'];

/*print_r($_GET);
die();*/

if(isset($product_id)){

    $query = "SELECT products.*, categories.category_name, brands.brand_name FROM products
              INNER JOIN categories ON products.category_id = categories.category_id
              INNER JOIN brands ON products.brand_id = brands.brand_id
              WHERE products.product_id = :product_id";

    $sth = $conn->prepare($query);
    $sth->bindParam('product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);
}
?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E - SHOP :: HOME</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<?php include_once('elements/nav.php');?>

<!-- Page Content -->
<div class="container">

    <hr>
    <hr>
    <h4 class="text-center">Product Detail</h4>
    <hr>

    <form method="post" action="add_to_cart.php">
    <!-- Portfolio Item Row -->
    <div class="row">

        <div class="col-md-8">
            <img class="img-fluid" src="../uploads/<?=$product['picture'];?>" alt="http://placehold.it/700x400">
        </div>

        <div class="col-md-4">
            <h3 class="title mb-3"><?=$product['product_name'];?></h3>
            <p class="price-detail-wrap">
	<span class="price h3 text-warning">
		<span class="currency">US $</span><span class="num"><?=$product['mrp'];?></span>
	</span>
                <span>/per piece</span>
            </p> <!-- price-detail-wrap .// -->
            <dt>Description</dt>
            <dd><p>Here goes description consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco </p></dd>

            <dt>Category</dt>
            <dd><?=$product['category_name'];?></dd>

            <dt>Brand</dt>
            <dd><?=$product['brand_name'];?></dd>
            <hr>

            <dt>Quantity:</dt>
            <input style="width:100px;" type="number" name="quantity" value="1" min="1">
                &nbsp;
            <input type="hidden" name="product_id" value="<?=$product['product_id'];?>">
            <button type="submit" class="btn btn-warning" name="add-to-cart">Add to Cart</button>
            <hr>
        </div>

    </div>
    <!-- /.row -->
    </form>

    <hr>

    <!-- Related Projects Row -->
    <h3 class="my-4">Related Products</h3>

    <div class="row">

        <div class="col-md-3 col-sm-6 mb-4">
            <a href="#">
                <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
            <a href="#">
                <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
            <a href="#">
                <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
        </div>

        <div class="col-md-3 col-sm-6 mb-4">
            <a href="#">
                <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->


        <!-- Footer -->
        <?php include_once('elements/footer.php');?>

        <!-- Bootstrap core JavaScript -->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>