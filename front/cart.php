<?php
session_start();

include_once("../src/db.php");


$s_id = $_SESSION['guest_user'];


/*if(array_key_exists('logged_in_user', $_SESSION) && !empty($_SESSION['logged_in_user'])){

}else{
    $_SESSION['logged_in_user'] = 'logged_in_user'.time();
    $s_id = $_SESSION['logged_in_user'];
}*/

//refreshing the page to load the cart properly
if(!isset($_GET['id'])){
    echo "<meta http-equiv='refresh' content='0;URL=?id=eshikhon'/>";
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E - SHOP :: HOME</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="../vendor/bootstrap/css/all.min.css">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<?php include_once('elements/nav.php');?>

<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">E-SHOP CART</h1>
    </div>
</section>

<div class="container mb-4">
    <div class="row">
        <div class="col-12">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th scope="col"> </th>
                        <th scope="col" class="text-center">Product</th>
                        <th scope="col" class="text-center">Price</th>
                        <th scope="col" class="text-center">Quantity</th>
                        <th> </th>
                        <th scope="col" class="text-center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(isset($s_id)){

                        $query = 'SELECT * FROM carts WHERE s_id = :s_id';
                        $sth = $conn->prepare($query);
                        $sth->bindParam(':s_id', $s_id);
                        $sth->execute();

                        $carts = $sth->fetchAll(PDO::FETCH_ASSOC);

                    }
                    $sub_total = 0;
                    $total_qty = 0;
                    if($carts){
                        foreach($carts as $cart){
                            $sub_total = $sub_total + $cart['total_price'];
                            $total_qty = $total_qty + $cart['quantity'];
                    ?>
                    <tr>
                        <td><img style="width: 60px; height: 50px;" src="../uploads/<?=$cart['picture'];?>" alt="https://dummyimage.com/50x50/55595c/fff"> </td>
                        <td class="text-center"><?=$cart['product_name']?></td>
                        <td class="text-center">US $<?= $cart['unit_price'];?></td>
                        <td>
                            <form action="manage_cart.php" method="post">
                                <div class="input-group">
                                    <input class="form-control" type="hidden" name="cart_id" value="<?= $cart['cart_id'];?>">
                                    <input class="form-control" type="hidden" name="product_id" value="<?= $cart['product_id'];?>">
                                    <input class="form-control" type="number" name="quantity" value="<?= $cart['quantity'];?>" min="1">
                                    <input class="form-control" type="hidden" name="unit_price" value="<?= $cart['unit_price'];?>">
                                    <td>
                                        <button type="submit" class="btn btn-sm btn-primary" name="update-cart"><i class="fas fa-pen-square"></i></button>
                                        <a href="manage_cart.php?cart_id=<?= $cart['cart_id'];?>" class="btn btn-sm btn-danger" name="remove-from-cart"><i class="fa fa-trash"></i></a>
                                    </td>
                                </div>
                            </form>
                        </td>
                        <td class="text-center">US $<?= $cart['total_price'];?></td>

                    </tr>
                    <?php } }else{ ?>

                        <tr>
                            <td colspan="6" class="jumbotron">There is no products available now! <a href="index.php" class="btn btn-success btn-sm">Click Here</a> to add one on the cart. </td>
                        </tr>
                    <?php } ?>

                    <?php
                        $_SESSION['$total_qty'] = $total_qty;
                    ?>
            
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Sub-Total</td>
                        <td class="text-center">US $<?=$sub_total;?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>Shipping</td>
                        <td class="text-center"><?php $shipping = 10; echo 'US $'.$shipping;?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td class="text-center"><strong>US $<?php if($sub_total == 0){echo $total = 0;}else{echo $total = ($sub_total + $shipping);}?></strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col mb-2">
            <div class="row">
                <div class="col-sm-12  col-md-6">
                    <a href="index.php" class="btn btn-block btn-outline-info">Continue Shopping</a>
                </div>
                <div class="col-sm-12 col-md-6 text-right">
                    <a href="order.php" class="btn btn-lg btn-block btn-success">Proceed to Checkout</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php include_once('elements/footer.php');?>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/popper.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../vendor/bootstrap/js/all.min.js"></script>

</body>

</html>