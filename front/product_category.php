<?php
session_start();
include_once("../src/db.php");

$category_id = $_GET['category_id'];

if(isset($category_id)){
    $query = "SELECT * FROM products WHERE category_id = :category_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':category_id', $category_id);
    $sth->execute();

    $productsByCategory = $sth->fetchAll(PDO::FETCH_ASSOC);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E - SHOP :: HOME</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="../vendor/bootstrap/css/all.min.css">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<?php include_once('elements/nav.php');?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">

            <?php include_once('elements/list.php'); ?>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <?php
            $query = "SELECT * FROM categories WHERE category_id = :category_id";
            $sth = $conn->prepare($query);
            $sth->bindParam(':category_id', $category_id);
            $sth->execute();

            $category = $sth->fetch(PDO::FETCH_ASSOC);
            ?>
            <hr>
            <div class="heading"><h3 class="text-center">Latest in <?=$category['category_name']?></h3></div>
            <hr>

<div class="row">




<?php
        foreach($productsByCategory as $product):
        ?>
        <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
                <form action="add_to_cart.php" method="post">
                    <a href="#"><img class="card-img-top" src="../uploads/<?=$product['picture'];?>" alt="http://placehold.it/700x400"></a>
                    <div class="card-body">
                        <h4 class="card-title"><?= $product['product_name'];?></h4>
                        <h5>$<?= $product['mrp'];?></h5>
                        <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
                        <input type="hidden" name="product_id" value="<?=$product['product_id'];?>">
                        <input style="width:100px;" type="hidden" name="quantity" value="1" min="1">
                        <button name="add-to-cart" class="btn btn-warning btn-sm" >Add to Cart</button>
                </form>
                <a href="product_detail.php?product_id=<?=$product['product_id'];?>" class="btn btn-info btn-sm">View Detail</a>
            </div>
            <div class="card-footer">
                <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
        </div>
    </div>
</div>
</div>

        <!-- Footer -->
        <?php include_once('elements/footer.php');?>

        <!-- Bootstrap core JavaScript -->
        <script src="../vendor/jquery/jquery.min.js"></script>
        <script src="../vendor/bootstrap/js/popper.min.js"></script>
        <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="../vendor/bootstrap/js/all.min.js"></script>

</body>

</html>
