<?php
session_start();

include_once('../src/db.php');

if(isset($_SESSION['id'])){
    header('Location:order.php');
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['login-btn'])){

    $email = $_POST['email'];
    $password = $_POST['password'];

    if(empty($email) && empty($password)){
        $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Fields must not be empty!</div>";
    }elseif(empty($email) || empty($password)){
        $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Field must not be empty!</div>";
    }else {

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Email address is not valid!</div>";
        }else{
            $query = 'SELECT email FROM customers WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);

            if(empty($row['email'])){
                $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Email address is not exist!</div>";
            }
        }

        if(empty($errors)){

            $query = 'SELECT * FROM customers WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $result = $sth->fetch(PDO::FETCH_ASSOC);

            if($result){

                $password_match = password_verify($password, $result['password']);

                if($password_match == true){

                    if(version_compare(phpversion(), '5.5.0', '<')){
                        if(empty(session_id())){
                            session_start();
                        }
                    }else{
                        if(session_status() == PHP_SESSION_NONE){
                            session_start();
                        }
                    }

                    $time_zone = date_default_timezone_set("Asia/Dhaka");

                    $_SESSION['logged_in_user'] = 'logged_in_user'.time();

                    $_SESSION['login'] = true;
                    $_SESSION['id'] = $result['id'];
                    $_SESSION['name'] = $result['name'];
                    $_SESSION['email'] = $result['email'];

                    $_SESSION['date-time'] = date('Y-m-d h:i:s');

                    $login_msg = "<div class='alert alert-success'><strong>Success!</strong> You are logged in.</div>";

                    $_SESSION['login-msg'] = $login_msg;
                    $_SESSION['$total_qty'] = true;

                    header('Location:order.php');

                }else{
                    $msgs[] = "<div class='alert alert-danger'><strong>Sorry!</strong> Data not found!</div>";

                }
            }
        }

    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E - SHOP :: HOME</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="../vendor/bootstrap/css/all.min.css">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<?php include_once('elements/nav.php');?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 18rem;">
        <img class="card-img-top mx-auto" style="width: 60%" src="../lib/img/login.png" alt="Login Icon">
        <div class="card-body">

            <?php
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }

            if(!empty($msgs)){
                foreach($msgs as $msg){
                    echo $msg;
                }
            }
            ?>

            <?php
            if(isset($_SESSION['updated'])){
                echo $_SESSION['updated'];
            }

            $_SESSION['updated'] = NULL;

            ?>

            <form action="" method="post">
                <div class="form-group">
                    <input type="email"
                           value="<?php if(!empty($errors) || !empty($msgs)){echo $_POST['email'];}?>"
                           name="email"
                           class="form-control"
                           id="email"
                           autofocus="autofocus"
                           placeholder="Enter Email">
                </div>

                <div class="form-group">
                    <input type="password"
                           name="password"
                           class="form-control"
                           id="password"
                           placeholder="Enter Password">
                </div>
                <button type="submit" class="btn btn-primary" name="login-btn"><i class="fas fa-sign-in-alt"></i> Login</button>
                <span><a href="register.php">Register</a></span>
            </form>
        </div>

        <div class="card-footer">
            <a href="forgot_password.php">Forgot Password?</a>
        </div>
    </div>
</div>


<!-- Footer -->
<?php include_once('elements/footer.php');?>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/popper.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../vendor/bootstrap/js/all.min.js"></script>

</body>

</html>