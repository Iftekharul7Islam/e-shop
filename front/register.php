<?php
session_start();

include_once('../src/db.php');

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['register-btn'])){

    $name = $_POST['name'];
    $password = $_POST['password'];
    $confirmPassword = $_POST['confirm-password'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $country = $_POST['country'];
    $zip = $_POST['country'];


    if(empty($name) && empty($password) && empty($confirmPassword) && empty($email) && empty($phone) && empty($address) && empty($city) && empty($country) && empty($country)){
        $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Fields must not be empty!</div>";
    }elseif(empty($name) || empty($password) || empty($confirmPassword) || empty($email) || empty($phone) || empty($address) || empty($city) || empty($country) || empty($country)){
        $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Field must not be empty!</div>";
    }else{

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Email address is not valid!</div>";
        }else{
            $query = 'SELECT email FROM customers WHERE email = :email';
            $sth = $conn->prepare($query);
            $sth->bindParam(':email', $email);
            $sth->execute();
            $row = $sth->fetch(PDO::FETCH_ASSOC);

            if(!empty($row['email'])){
                $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Email address is already exist!</div>";
            }
        }

        if(strlen($password) < 6){
            $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Password is too short!</div>";
        }

        if(strlen($password) > 32){
            $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Password is too long!</div>";
        }

        if($password != $confirmPassword){
            $errors[] = "<div class='alert alert-danger'><strong>Error!</strong> Passwords do not match!</div>";
        }

        if(empty($errors)){

            $hashedPassword = password_hash($password, PASSWORD_BCRYPT);

            $query = 'INSERT INTO customers(name, password, email, phone, address, city, country, zip) 
                      VALUES (:name, :password, :email, :phone, :address, :city, :country, :zip)';

            $sth = $conn->prepare($query);
            $sth->bindParam(':name', $name);
            $sth->bindParam(':password', $hashedPassword);
            $sth->bindParam(':email', $email);
            $sth->bindParam(':phone', $phone);
            $sth->bindParam(':address', $address);
            $sth->bindParam(':city', $city);
            $sth->bindParam(':country', $country);
            $sth->bindParam(':zip', $zip);
            $result = $sth->execute();

            //print_r($result);
            //die();

            if($result){

                $msgs[] = "<div class='alert alert-success'><strong>Thank You!</strong> Your registration is successful.</div>";
            }else{
                $msgs[] = "<div class='alert alert-danger'><strong>Sorry!</strong> There are some problem with your details!</div>";
            }

        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E - SHOP :: HOME</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="../vendor/bootstrap/css/all.min.css">

    <!-- Custom styles for this template -->
    <link href="../css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<?php include_once('elements/nav.php');?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 30rem;">
        <div class="card-header">Registration</div>
        <div class="card-body">
            <?php
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }

            if(!empty($msgs)){
                foreach($msgs as $msg){
                    echo $msg;
                }
            }
            ?>
            <form action="" method="post">

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['name'];}?>"
                           name="name"
                           class="form-control"
                           id="name"
                           autofocus="autofocus"
                           placeholder="Enter Name">
                </div>

                <div class="form-group">
                    <input type="email"
                           value="<?php if(!empty($errors)){echo $_POST['email'];}?>"
                           name="email"
                           class="form-control"
                           id="email"
                           placeholder="Enter Email">
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['phone'];}?>"
                           name="phone"
                           class="form-control"
                           id="phone"
                           autofocus="autofocus"
                           placeholder="Enter Phone">
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['address'];}?>"
                           name="address"
                           class="form-control"
                           id="address"
                           autofocus="autofocus"
                           placeholder="Enter Address">
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['city'];}?>"
                           name="city"
                           class="form-control"
                           id="city"
                           autofocus="autofocus"
                           placeholder="Enter City">
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['country'];}?>"
                           name="country"
                           class="form-control"
                           id="country"
                           autofocus="autofocus"
                           placeholder="Enter Country">
                </div>

                <div class="form-group">
                    <input type="text"
                           value="<?php if(!empty($errors)){echo $_POST['zip'];}?>"
                           name="zip"
                           class="form-control"
                           id="zip"
                           autofocus="autofocus"
                           placeholder="Enter Zip - Code">
                </div>

                <div class="form-group">
                    <input type="password"
                           name="password"
                           class="form-control"
                           id="password"
                           placeholder="Enter Password">
                </div>

                <div class="form-group">
                    <input type="password"
                           name="confirm-password"
                           class="form-control"
                           id="password"
                           placeholder="Confirm Password">
                </div>

                <button type="submit" class="btn btn-primary" name="register-btn"><i class="fas fa-user">&nbsp;</i> Register</button>
                <span><a href="login.php">Login</a></span>
            </form>
        </div>

        <div class="card-footer">
        </div>
    </div>
</div>



<!-- Footer -->
<?php include_once('elements/footer.php');?>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/popper.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="../vendor/bootstrap/js/all.min.js"></script>

</body>

</html>