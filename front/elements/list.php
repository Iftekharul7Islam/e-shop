<?php
$query = "SELECT * FROM categories";
$sth = $conn->prepare($query);
$sth->execute();

$categories = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<h1 class="my-2">E - SHOP</h1>

<?php
foreach($categories as $category):
?>
<div class="list-group">
    <a href="product_category.php?category_id=<?=$category['category_id']?>" class="list-group-item"><?=$category['category_name'];?></a>
</div>
<?php
endforeach;
?>