<?php
$query = "SELECT * FROM products ORDER BY product_id DESC";
$sth = $conn->prepare($query);
$sth->execute();

$products = $sth->fetchAll(PDO::FETCH_ASSOC);

/*print_r($products);
die();*/
?>
<?php
foreach($products as $product):
?>
<div class="col-lg-4 col-md-6 mb-4">
    <div class="card h-100">
        <form action="add_to_cart.php" method="post">
        <a href="#"><img class="card-img-top" src="../uploads/<?=$product['picture'];?>" alt="http://placehold.it/700x400"></a>
        <div class="card-body">
            <h4 class="card-title"><?= $product['product_name'];?></h4>
            <h5>$<?= $product['mrp'];?></h5>
            <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet numquam aspernatur!</p>
            <input type="hidden" name="product_id" value="<?=$product['product_id'];?>">
            <input style="width:100px;" type="hidden" name="quantity" value="1" min="1">
            <button name="add-to-cart" class="btn btn-warning btn-sm" >Add to Cart</button>
        </form>
            <a href="product_detail.php?product_id=<?=$product['product_id'];?>" class="btn btn-info btn-sm">View Detail</a>
        </div>
        <div class="card-footer">
            <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
        </div>
    </div>
</div>
<?php endforeach; ?>