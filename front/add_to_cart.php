<?php
session_start();

include_once("../src/db.php");

/*print_r($_POST);
die();*/

$product_id = $_POST['product_id'];

if(!empty($product_id)){

    $query = 'SELECT * FROM products WHERE product_id = :product_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);

    if($_SERVER['REQUEST_METHOD'] = 'POST' && isset($_POST['add-to-cart'])){

        $s_id = $_SESSION['guest_user'];
        $product_id = $product['product_id'];
        $product_name = $product['product_name'];
        $picture = $product['picture'];
        $unit_price = $product['mrp'];
        $quantity = $_POST['quantity'];
        $total_price = $unit_price * $quantity;

        if(!empty($s_id) && !empty($product_id)){

            $query = 'SELECT * FROM carts WHERE s_id = :s_id AND product_id = :product_id';
            $sth = $conn->prepare($query);
            $sth->bindParam(':s_id', $s_id);
            $sth->bindParam(':product_id', $product_id);
            $sth->execute();

            $cart_row = $sth->fetch(PDO::FETCH_ASSOC);

            if($cart_row > 0){

                //to avoid adding the same product

                $final_quantity = $quantity + $cart_row['quantity'];
                $total_price = $final_quantity * $cart_row['unit_price'];

                $query = "UPDATE carts
                          SET 
                              quantity = :quantity, 
                              total_price = :total_price
                              
                          WHERE s_id = :s_id AND product_id = :product_id";


                $sth = $conn->prepare($query);
                $sth->bindParam(':quantity', $final_quantity);
                $sth->bindParam(':total_price', $total_price);
                $sth->bindParam(':s_id', $s_id);
                $sth->bindParam(':product_id', $product_id);
                $result = $sth->execute();

                if($result){

                    header('Location:cart.php');
                }


            }else{

                //add new product

                $query = "INSERT INTO 
                  carts (s_id, product_id, product_name, picture, unit_price, quantity, total_price) 
                  VALUES (:s_id, :product_id, :product_name, :picture, :unit_price, :quantity, :total_price)";

                $sth = $conn->prepare($query);
                $sth->bindParam(':s_id', $s_id);
                $sth->bindParam(':product_id', $product_id);
                $sth->bindParam(':product_name', $product_name);
                $sth->bindParam(':picture', $picture);
                $sth->bindParam(':unit_price', $unit_price);
                $sth->bindParam(':quantity', $quantity);
                $sth->bindParam(':total_price', $total_price);
                $result = $sth->execute();

                if($result){
                    header('Location:cart.php');

                }

            }
        }

    }

}