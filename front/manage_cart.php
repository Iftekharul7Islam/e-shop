<?php
session_start();

include_once("../src/db.php");


$s_id = $_SESSION['guest_user'];

if(isset($_POST['update-cart'])){

    /*echo '<pre>';
    print_r($_POST);
    echo '<pre>';
    die();*/

    $cart_id = $_POST['cart_id'];
    $product_id = $_POST['product_id'];
    $unit_price = $_POST['unit_price'];
    $quantity = $_POST['quantity'];

    $final_quantity = $quantity;
    $total_price = $final_quantity * $unit_price;

    $query = "UPDATE carts
          SET 
             quantity = :quantity, 
             total_price = :total_price
                              
          WHERE cart_id = :cart_id";

    $sth = $conn->prepare($query);
    $sth->bindParam(':quantity', $final_quantity);
    $sth->bindParam(':total_price', $total_price);
    $sth->bindParam(':cart_id', $cart_id);
    $result = $sth->execute();

    if($result){
        $_SESSION['updated'] = '<div class="alert alert-success"><strong>Success! </strong>Cart product updated successfully.</div>';
        header('Location:cart.php');
    }else{
        $_SESSION['updated'] = '<div class="alert alert-danger"><strong>Error! </strong>Product is not updated!</div>';
        header('Location:cart.php');
    }
}


$cart_id = $_GET['cart_id'];

if(isset($cart_id)){

    $query = "DELETE FROM carts WHERE cart_id = :cart_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':cart_id', $cart_id);
    $result = $sth->execute();

    if($result){
        $_SESSION['deleted'] = '<div class="alert alert-success"><strong>Success! </strong>Cart product removed successfully.</div>';
        header('Location:cart.php');
    }else{
        $_SESSION['deleted'] = '<div class="alert alert-danger"><strong>Error! </strong>Your product still on the cart!</div>';
        header('Location:cart.php');
    }
}