<?php
session_start();
include_once("../src/db.php");

$query = "SELECT * FROM banners ORDER BY banner_id DESC";
$sth = $conn->prepare($query);
$sth->execute();

$banners = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">

</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php include_once('elements/sidebar.php'); ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php include_once('elements/nav.php');?>

        <div class="container-fluid">
            <div class="container" style="margin-top: 50px">
                <div class="card mx-auto" style="width: 100%">
                    <div class="card-header">Banners</div>
                    <div class="card-body">
                        <div style="float: right">
                            <a href="add_banners.php" class="btn btn-primary btn-sm"><i class="far fa-plus-square"></i> Add</a>
                        </div>
                        <?php
                        if(isset($_SESSION['inserted'])){
                            echo $_SESSION['inserted'];
                        }
                        $_SESSION['inserted'] = NULL;

                        ?>
                        <?php
                        if(isset($_SESSION['updated'])){
                            echo $_SESSION['updated'];
                        }
                        $_SESSION['updated'] = NULL;

                        ?>

                        <?php
                        if(isset($_SESSION['deleted'])){
                            echo $_SESSION['deleted'];
                        }
                        $_SESSION['deleted'] = NULL;

                        ?>
                        <table class="table table-hover table-bordered">

                            <thead>
                            <tr>
                                <th scope="col">Sl No.</th>
                                <th scope="col">Banner Picture</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>

                            <tbody>
                            <?php
                            if($banners){
                                $i = 0;
                                foreach($banners as $banner){
                                    $i = $i + 1;
                                    ?>

                                    <tr>
                                        <th scope="row"><?= $i; ?></th>
                                        <td>
                                            <img style="width: 100px;"
                                                 src="../uploads/<?= $banner['picture'];?>" alt="Banner Picture">
                                        </td>
                                        <td><a href="edit_banners.php?banner_id=<?= $banner['banner_id']; ?>"
                                               class="btn btn-info btn-sm">Edit</a>
                                            <a onclick="return confirm('Are you sure you want to delete?')"
                                               href="delete_banners.php?banner_id=<?= $banner['banner_id']; ?>"
                                               class="btn btn-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>

                                    <?php }}else{ ?>
                                <tr >
                                    <td colspan="3">No Banner is available!<a href="add_banners.php">Click Here</a>to add a banner. </td>
                                </tr>

                            <?php } ?>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script src="../js/main.js"></script>

</body>

</html>
