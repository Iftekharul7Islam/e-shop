<?php
session_start();
include_once('../src/db.php');

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['add-banner'])){

    $time_zone = date_default_timezone_set("Asia/Dhaka");

    $target_file = $_FILES['picture']['tmp_name'];
    //$filename = time().'_'.str_replace(' ', '-',$_FILES['picture']['name']);
    $filename = $_FILES['picture']['name'];
    $destination_file = $_SERVER['DOCUMENT_ROOT'].'/e-commerce/uploads/'.$filename;

    $uploaded = move_uploaded_file($target_file, $destination_file);
    //die($uploaded);

    if($uploaded){
        $destination_filename = $filename;
    }else{
        $destination_filename = "";
    }

    if(!empty($destination_filename)){

        $query = "INSERT INTO banners(picture) 
                      VALUES(:picture)";

        $sth = $conn->prepare($query);
        $sth->bindParam(':picture', $destination_filename);
        $result = $sth->execute();

        if($result){
            $_SESSION['inserted'] = "<div class='alert alert-success'>Banner inserted successfully.</div>";
            header("location:manage_banners.php");
        }else{
            $_SESSION['inserted'] = "<div class='alert alert-danger'>Banner not inserted!</div>";
        }

    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">

</head>
<body>
<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php include_once('elements/sidebar.php'); ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php include_once('elements/nav.php');?>
        <div class="container" style="margin-top: 50px">
            <div class="card mx-auto" style="width: 30rem;">
                <div class="card-header">Add Brand</div>
                <div class="card-body">

                   <?php
                    //check for any errors
                    if(!empty($errors)){
                        foreach($errors as $error){
                            echo $error;
                        }
                    }
                    ?>

                    <form action="" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <input type="file"
                                   name="picture"
                                   id="picture"
                                   class="form-control">
                        </div>

                        <button type="submit" class="btn btn-primary" name="add-banner"><i class="fas fa-sign-in-alt"></i> Add</button>
                    </form>
                </div>
            </div>
        </div>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script src="../js/main.js"></script>

</body>

</html>