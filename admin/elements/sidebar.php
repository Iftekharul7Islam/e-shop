<div class="bg-light border-right" id="sidebar-wrapper">
    <div class="sidebar-heading">E - SHOP </div>
    <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action bg-light">Dashboard</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Profile</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Customers</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Categories</a>
        <a href="manage_banners.php" class="list-group-item list-group-item-action bg-light">Banners</a>
        <a href="manage_products.php" class="list-group-item list-group-item-action bg-light">Products</a>
    </div>
</div>