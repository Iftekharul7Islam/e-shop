<?php
session_start();
include_once("../src/db.php");

$product_id = $_GET['product_id'];
if(isset($product_id)){

    $query = 'SELECT * FROM products WHERE product_id = :product_id';
    $sth = $conn->prepare($query);
    $sth->bindParam(':product_id', $product_id);
    $sth->execute();

    $product = $sth->fetch(PDO::FETCH_ASSOC);
}

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['update-product'])){

    $product_name = $_POST['product_name'];
    $category_id = $_POST['category_id'];
    $brand_id = $_POST['brand_id'];
    $mrp = $_POST['mrp'];

    $is_active = 0;

    if(array_key_exists('is_active', $_POST)){
        $is_active = $_POST['is_active'];
    }

    $prev_picture_name = $_POST['prev_picture_name'];

    $time_zone = date_default_timezone_set("Asia/Dhaka");

    $uploaded = false;

    if($_FILES['picture']['size'] > 0){
        $target_file = $_FILES['picture']['tmp_name'];
        //$filename = time().'_'.str_replace(' ', '-',$_FILES['picture']['name']);
        $filename = $_FILES['picture']['name'];
        $destination_file = $_SERVER['DOCUMENT_ROOT'].'/e-commerce/uploads/'.$filename;

        $uploaded = move_uploaded_file($target_file, $destination_file);
    }

    if($uploaded){
        $destination_filename = $filename;
    }else{
        $destination_filename = $prev_picture_name;
    }

    if(empty($product_name) && empty($category_id) && empty($brand_id) && empty($mrp)){
        $errors[] = "<div class='alert alert-danger'>Fields must not be empty!</div>";
    }elseif(empty($product_name) || empty($category_id) || empty($brand_id) || empty($mrp)){
        $errors[] = "<div class='alert alert-danger'>Field must not be empty!</div>";
    }else{

        $time_zone = date_default_timezone_set("Asia/Dhaka");

        $query = "UPDATE products
SET 
product_name = :product_name,
picture = :picture,
category_id = :category_id,
brand_id = :brand_id,
mrp = :mrp,
is_active = :is_active,
modified_at = :modified_at
WHERE product_id = :product_id";

        $sth = $conn->prepare($query);
        $sth->bindParam(':product_id', $product_id);
        $sth->bindParam(':product_name', $product_name);
        $sth->bindParam(':picture', $destination_filename);
        $sth->bindParam(':category_id', $category_id);
        $sth->bindParam(':brand_id', $brand_id);
        $sth->bindParam(':mrp', $mrp);
        $sth->bindParam(':is_active', $is_active);
        $sth->bindParam(':modified_at', date('Y-m-d h:i:s'));
        $result = $sth->execute();

        if($result){
            session_start();

            $_SESSION['updated'] = "<div class='alert alert-success'>Product updated successfully.</div>";
            header("location:manage_products.php");
        }else{
            $_SESSION['updated'] = "<div class='alert alert-danger'>Product not updated!</div>";
        }


    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/simple-sidebar.css" rel="stylesheet">

</head>

<body>

<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <?php include_once('elements/sidebar.php'); ?>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">

        <?php include_once('elements/nav.php');?>

<div class="container" style="margin-top: 50px">
    <div class="card mx-auto" style="width: 30rem;">
        <div class="card-header"><h5>Edit Product</h5></div>
        <div class="card-body">

            <?php
            //check for any errors
            if(!empty($errors)){
                foreach($errors as $error){
                    echo $error;
                }
            }
            ?>

            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <input
                        type="text"
                        onclick="this.select()"
                        value="<?= $product['product_name'];?>"
                        name="product_name"
                        class="form-control"
                        id="product_name"
                        placeholder="Enter Product Name"
                        autofocus="autofocus">
                </div>

                <div class="form-group">
                    <input
                        type="hidden"
                        name="prev_picture_name"
                        value="<?= $product['picture']; ?>"
                        class="form-control"
                        id="prev_picture_name">
                </div>

                <div class="form-group">
                    <input
                        type="file"
                        onclick="this.select()"
                        value=""
                        name="picture"
                        class="form-control"
                        id="picture">
                    <?php if(!empty($product['picture'])){ ?>
                        <img class="card-img-top mx-auto"
                             style="width: 50%;"
                             src="../uploads/<?=$product['picture'];?>" alt="Product Picture">
                    <?php }else{ ?>
                        <div>No image is available. Please upload one.</div>
                    <?php }?>
                </div>

                <div class="form-group">
                    <select
                        name="category_id"
                        id="category_id"
                        class="form-control">
                        <option>Select Category...</option>
                        <?php
                        $query = "SELECT * FROM categories ORDER BY category_id DESC";
                        $sth = $conn->prepare($query);
                        $sth->execute();
                        $categories = $sth->fetchAll(PDO::FETCH_ASSOC);

                        if($categories){
                            foreach($categories as $category){
                                ?>
                                <option
                                    <?php
                                    if($product['category_id'] == $category['category_id']){
                                        ?>
                                        selected = "selected"
                                    <?php }?>
                                    value="<?= $category['category_id'];?>"><?= $category['category_name'];?>
                                </option>
                            <?php }}?>
                    </select>

                </div>

                <div class="form-group">
                    <select
                        name="brand_id"
                        id="brand_id"
                        class="form-control">
                        <option>Select Brand...</option>
                        <?php
                        $query = "SELECT * FROM brands ORDER BY brand_id DESC";
                        $sth = $conn->prepare($query);
                        $sth->execute();
                        $brands = $sth->fetchAll(PDO::FETCH_ASSOC);

                        if($brands){
                            foreach($brands as $brand){
                                ?>
                                <option
                                    <?php
                                    if($product['brand_id'] == $brand['brand_id']){
                                        ?>
                                        selected = "selected"
                                    <?php }?>
                                    value="<?= $brand['brand_id'];?>"><?= $brand['brand_name'];?>
                                </option>
                            <?php }}?>
                    </select>
                </div>

                <div class="form-group">
                    <input
                        type="text"
                        onclick="this.select()"
                        value="<?= $product['mrp'];?>"
                        name="mrp"
                        class="form-control"
                        id="mrp"
                        placeholder="Enter Price(MRP)">
                </div>

                <?php
                $checked = '';
                if($product['is_active']){
                    $checked = 'checked="checked"';
                }
                ?>


                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <label>Active &nbsp;</label>
                            <input type="checkbox"
                                   <?=$checked;?>
                                   value="1"
                                   class="form-control"
                                   name="is_active">
                        </div>
                    </div>

                </div>

                <button type="submit" class="btn btn-primary" name="update-product">Update</button>
            </form>


        </div>
    </div>
</div>



<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Menu Toggle Script -->
<script src="../js/main.js"></script>

</body>

</html>