<?php
session_start();
include_once("../src/db.php");

$banner_id = $_GET['banner_id'];

if(isset($brand_id)){
    $query = "DELETE FROM banners WHERE banner_id = :banner_id";
    $sth = $conn->prepare($query);
    $sth->bindParam(':banner_id', $banner_id);
    $result = $sth->execute();

    if($result){
        $_SESSION['deleted'] = "<div class='alert alert-success'>Banner deleted successfully.</div>";
        header("location:manage_banner.php");
    }else{
        $_SESSION['deleted'] = "<div class='alert alert-danger'>Banner is not deleted!</div>";
    }
}



