<?php

//connect to database
$servername = "localhost";
$dbusername = "root";
$dbpassword = "";
$dbname = "eshop";

//connection string : connecting to database
try{
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $dbusername, $dbpassword);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
    echo 'Connection failed'.$e->getMessage();
}